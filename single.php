<?php get_header(); ?>
            <article class="theme-article">
              <div class="col-md-8" id="content">
                
                  <div class="posts-list" >
                    <?php get_template_part('template-part/post/post');?>
                  </div>
                  <div class="comments">
                    <?php
                      if ( comments_open() || get_comments_number() ) :
                       comments_template();
                       endif;
                      ?>
                  </div>
                  
              </div>
            </article>
            <sidebar>
                <div class="col-md-2">
                    <?php get_template_part('template-part/right');?>
                </div>
            </sidebar>
        </div>
      </div>
<?php get_footer(); ?>