<div class="sidebar right-sidebar hidden-xs">
    <div class="right-widget">
        <h3>关键词搜索</h3>
        <form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="">
                <input type="text" value="" name="s" id="s" class="center-block" placeholder="你造吗？这是一个搜索框">
			</div>
    	</form>
	</div>
<?php if(is_dynamic_sidebar()) dynamic_sidebar('right-sidebar');?>
</div>