<?php while ( have_posts() ) : the_post(); ?>
                    <div class="item">
                      <header>
                        <h3 class="entry-title"><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h3>
                      </header>
                      <div class="item-content">
                        <a href="<?php the_permalink(); ?>" class="thumbnail-link hidden-xs"><img src="<?php post_thumbnail_src(); ?>" alt="<?php the_title(); ?>" width="195" height="125" class="thumbnail img-thumbnail img-responsive"></a>
                        <div class="more">
                          <p><?php echo mb_strimwidth(strip_shortcodes(strip_tags(apply_filters('the_content', $post->post_content))), 0, 300,"...");
                          ?></p>
                        </div>
                      </div>
                      <div class="post-meta">
                        <time>
                            <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                            <?php echo timeago( get_gmt_from_date(get_the_time('Y-m-d G:i:s')) )?>   
                        </time><span class="hidden-xs">&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span>&nbsp;&nbsp;<?php the_category(',')?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>&nbsp;<?php the_views(); ?>&nbsp;&nbsp;&nbsp;&nbsp;
                        <span class="hidden-xs pull-right read-more glyphicon glyphicon-hand-right"><a href="<?php the_permalink() ?>">阅读全文</a></span>
                      </div>
                    </div>
                <?php endwhile; ?>