<?php while ( have_posts() ) : the_post(); ?>
<div class="item">
  <header>
    <h2 class="entry-title post-title"><?php the_title();?></h2>
  </header>
  <div class="single-meta">
    <time><?php echo timeago( get_gmt_from_date(get_the_time('Y-m-d G:i:s')) )?>  </time>&nbsp;.&nbsp;<?php the_category(',')?>&nbsp;.&nbsp;<?php the_tags('', ',', '');?>
  </div>
  <div class="item-content post-content">
    <?php the_content();?>
  </div>
</div>
<?php endwhile; ?>