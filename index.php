<?php get_header(); ?>
            <article class="theme-article">
              <div class="col-md-8" id="content">
                  <div class="posts-list">
                    <?php get_template_part('template-part/content');?>
                  </div>
                  
                  <div class="page_navi">
                  	<?php the_posts_pagination( array(
                    'prev_text' => 'Previous page',
                    'next_text' => 'Next page',
                    'prev_next' => false,
                    'before_page_number' => '',
                ) );?>
                  </div>
              </div>
            </article>
            <sidebar>
                <div class="col-md-2">
                    <?php get_template_part('template-part/right');?>
                </div>
            </sidebar>
        </div>
      </div>
<?php get_footer(); ?>