(function($) {
    //文章图片自动添加链接到原图
        $('.post-content img').each(function(i){  
            if (! this.parentNode.href) {  
                $(this).wrap("<a href='"+this.src+"' class='fancybox' onclick='return hs.expand(this);'></a>");  
            }  
        });  

        //灯箱初始化
        $(".fancybox").fancybox();
})(jQuery);