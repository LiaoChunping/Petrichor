# Petrichor - WordPressTheme

#### 项目介绍
Petrichor是WordPress主题，黑白灰设计，三栏，响应式布局

### License
The Petrichor Html,CSS,JavaScript,and PHP files are licensed under the GNU General Public License v2 or later:

http://www.gnu.org/licenses/gpl-2.0.html

The Kratos documentation is licensed under the CC BY-NC-SA 4.0 License:

https://creativecommons.org/licenses/by-nc-sa/4.0/