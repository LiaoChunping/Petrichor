<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <?php wp_head(); ?>
  </head>
  <body>
        <div class="container-fluid">
          <div class="row">
            <header class="theme-header">
              <div class="col-md-2">
                <?php get_template_part('template-part/left');?>
              </div>
            </header>